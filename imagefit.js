/* jquery.imagefit 
 *
 * Version 0.2 by Oliver Boermans <http://www.ollicle.com/eg/jquery/imagefit/>
 *
 * Extends jQuery <http://jquery.com>
 *
 */
(function($) {
	$.fn.imagefit = function(options) {
		var fit = {
			all : function(imgs){
				imgs.each(function(){
					fit.one(this);
					})
				},
			one : function(img){
				$(img)
					.width('100%').each(function()
					{
						$(this).height(Math.round(
							$(this).attr('startheight')*($(this).width()/$(this).attr('startwidth')))
						);
					})
				}
		};
		
		this.each(function(){
				var container = this;
				
				// store list of contained images (excluding those in tables)
				var imgs = $('img', container).not($("table img"));
				
				// store initial dimensions on each image 
				imgs.each(function(){
					$(this).attr('startwidth', $(this).width())
						.attr('startheight', $(this).height())
						.css('max-width', $(this).attr('startwidth')+"px");
				
					fit.one(this);
				});
				// Re-adjust when window width is changed
				$(window).bind('resize', function(){
					fit.all(imgs);
				});
			});
		return this;
	};
	
	$(document).delegate(".toggler", 'click', function(){
		if($(window).width() < 959){
			var collapser = $('#collapse-'+this.id);
			var visible = collapser.is(":visible");
			$('.toggler-content').hide('slow');
			if(visible==false){
				collapser.show('slow');
			}
		}
	});
	
	$(document).delegate(".footer-toggler", 'click', function(){
		if($(window).width() < 959){
			var collapser = $('#collapse-'+this.id);
			var visible = collapser.is(":visible");
			$('.footer-toggler-content').hide('slow');
			if(visible==false){
				collapser.show('slow');
			}
		}
	});
		
	$(document).delegate(".toggler-search", 'click', function(){
		$(".toggler-nav").removeClass('clicked');
		$('#mobile-nav-block').slideUp();
		$('#mobile-search-block').slideToggle();
		$('.toggler-search').toggleClass('clicked');
	});
		
	$(document).delegate(".toggler-nav", 'click', function(){
		$(".toggler-search").removeClass('clicked');
		$('#mobile-search-block').slideUp();
		$('#mobile-nav-block').slideToggle();
		$('.toggler-nav').toggleClass('clicked');
	});
		
	$(document).delegate("#mobile-app-toggler", 'click', function(){
		$('#mobile-app').slideUp();
	});

	$(document).delegate(".menu-toggler", 'click', function(e){
		var submenu = $(this).children(".toggler-submenu");
		var visible = submenu.is(":visible");
		
		if(visible==true){
			submenu.slideUp('fast');
			$(this).removeClass('active');
		} else {
			var togsiblings = $(this).siblings('.menu-toggler');
			var togsibsubs = togsiblings.children('.toggler-submenu');
			togsibsubs.slideUp('fast');
			togsiblings.removeClass('active');
			submenu.slideDown('fast');
			$(this).addClass('active');
		}
	
		e.stopPropagation();
	});

	$(window).load(function(){
		var imgfitClasses = Drupal.settings.fitImagefitClasses;
		$.each(imgfitClasses, function(classes, selector){
			$(selector).imagefit();
		});
	});
})(jQuery);